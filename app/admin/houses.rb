ActiveAdmin.register House do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :address, :landmark, :description, :authentication_token, 
                location_attributes: [ :id, :latitude, :longitude ],
                house_sections_attributes: [ :id, :name, :_destroy ]

  actions :show, :index, :edit

  index do
    selectable_column
    id_column
    column :name
    column :address
    column :landmark
    column :location do |house|
      "#{house.try(:location).try(:latitude)} , #{house.try(:location).try(:longitude)} "
    end
  end

  show do |house|
    attributes_table do
      row :name
      row :address
      row :landmark
      row :description
    end

    panel 'Location' do
      attributes_table_for house.location do
        row :longitude
        row :latitude
      end
    end

    attributes_table do
      row :house_sections do |house|
        bullet_list(house.house_sections.map(&:name), nil)
      end
    end
    
  end

  form do |f|
    f.inputs 'Authentication Details (Keep it Secret)' do
      f.semantic_errors *f.object.errors.keys

      f.input :authentication_token
      
      f.actions
    end
  end
end