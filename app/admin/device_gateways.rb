ActiveAdmin.register DeviceGateway do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


  permit_params :name, :base_url

  actions :show, :index

  index do
    selectable_column
    id_column
    column :name
    column :base_url
    column :device_gateway_url
    
    actions
  end

  show do |end_device|
    attributes_table do
      row :name
      row :base_url
      row :device_gateway_url
    end
  end

  form do |f|
    f.inputs 'End Device Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :base_url
      
      f.actions
    end
  end

end
