ActiveAdmin.register EndDeviceInfo do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :description, :model, :manufacturer,
                infra_parameter_ids: []

  actions :show, :index

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :model
    column :manufacturer
  end

  show do |end_device_info|
    attributes_table do
      row :name
      row :description
      row :model
      row :manufacturer

      row "Supported Infra Parameters" do |end_device_info|
        bullet_list(end_device_info.infra_parameters.map(&:name), nil)
      end
    end
  end

  form do |f|
    f.inputs 'End Device Information Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :description
      f.input :model
      f.input :manufacturer

      f.inputs " Infra Parameters " do
        f.input :infra_parameters, :as => :check_boxes
      end 


      f.actions
    end
  end
end
