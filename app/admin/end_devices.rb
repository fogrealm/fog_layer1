ActiveAdmin.register EndDevice do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  
  permit_params :name, :end_device_class, :should_alert_status, :serial_number,
                :device_url, :end_device_info_id, :house_section_id, :device_gateway_id,
                :service_infra_id

  actions :show, :index

  index do
    selectable_column
    id_column
    column :name
    column :service_infra do |end_device|
      " #{end_device.try(:service_infra).try(:name)} "
    end
    column :end_device_info do |end_device|
      " #{end_device.try(:end_device_info).try(:name)} "
    end
    column :house_section do |end_device|
      " #{end_device.try(:house_section).try(:name)} "
    end

    actions
  end

  show do |end_device|
    attributes_table do
      row :name
      #row :end_device_class
      row :serial_number
      
      row "Service Infra" do |end_device|
        end_device.try(:service_infra).try(:name)
      end

      row "House Section" do |end_device|
        end_device.try(:house_section).try(:name)
      end

      row :serial_number
      row :device_gateway
      row :should_alert_status

    end

    panel " End Device Information " do
      attributes_table_for end_device.end_device_info do
        row :name
        row :description
        row :model
        row :manufacturer

        row "Supported Infra Parameters" do |end_device_info|
          bullet_list(end_device_info.infra_parameters.map(&:name), nil)
        end
      end
    end
  end

  form do |f|
    f.inputs 'End Device Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      
      selected_end_device_info = f.object.new_record? ? EndDeviceInfo.try(:first) : f.object.end_device_info
      f.input :end_device_info, as: :select, collection: EndDeviceInfo.all, selected: selected_end_device_info.try(:id)

      f.input :serial_number

      selected_house_section = f.object.new_record? ? HouseSection.try(:first) : f.object.house_section
      f.input :house_section, as: :select, collection: HouseSection.all, selected: selected_house_section.try(:id)
      
      selected_service_infra = f.object.new_record? ? ServiceInfra.try(:first) : f.object.service_infra
      f.input :service_infra, as: :select, collection: ServiceInfra.all, selected: selected_service_infra.try(:id)

      selected_device_gateway = f.object.new_record? ? DeviceGateway.try(:first) : f.object.device_gateway
      f.input :device_gateway, as: :select, collection: DeviceGateway.all, selected: selected_device_gateway.try(:id)


      f.input :should_alert_status

      f.actions
    end
  end

end
