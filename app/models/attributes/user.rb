module Attributes::User
  
  def name
    self.profile.try(:name)
  end

end