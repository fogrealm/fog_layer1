module Attributes::DeviceGateway

  def generate_simulated_data(opts = {})
    service_infra_scenario = opts[:service_infra_scenario]
    data_retrieve_period = opts[:data_retrieve_period]

    if service_infra_scenario == ServiceInfra::SERVICE_INFRA_SCENARIO_NORMAL_ALL
      generate_simulated_data_scenario_normal_all(opts)
    elsif service_infra_scenario == ServiceInfra::SERVICE_INFRA_SCENARIO_FIRE_OCCURS
      generate_simulated_data_scenario_fire_occurs(opts)
    end
  end


  def generate_simulated_data_scenario_normal_all(opts = {})

    service_infra_scenario = opts[:service_infra_scenario]
    data_retrieve_period = opts[:data_retrieve_period]

    simulated_data = {}

    end_devices = self.end_devices
    end_devices_array = []

    simulated_data[:house] = end_devices.first.try(:house).try(:as_json) || House.first.try(:as_json)
    simulated_data[:gateway_timestamp] = Time.now.to_i

    end_devices.each do |end_device|
      end_device_map = {}  
      
      end_device_map[:id] = end_device.id
      end_device_map[:name] = end_device.name
      end_device_map[:house_section] = end_device.try(:house_section).try(:as_json)
      
      #end_device_measurements = {}

      end_device_id = end_device.id

      end_device_measurements_array = []

      end_device.infra_parameters.each do |infra_parameter|
        
        measurement = nil
        infra_parameter_name = infra_parameter.name
        infra_parameter_unit = infra_parameter.unit

        if infra_parameter_name == "Temperature"
          
          measurement = rand(25.00 .. 30.00).round(2)         
          #end_device_measurements[:temperature] = measurement
        
        elsif infra_parameter_name == "CO" 
        
          measurement = rand(0.00 .. 2.00).round(2)
          #end_device_measurements[:co] = measurement
        
        elsif infra_parameter_name == "CO2"
        
          measurement = rand(200..500)
          #end_device_measurements[:co2] = measurement
        
        elsif infra_parameter_name == "MOX GS822"
        
          measurement = rand(0.91 .. 0.99).round(2)
          #end_device_measurements[:mox_gs822] = measurement
        
        elsif infra_parameter_name == "MOX TGS880"
        
          measurement = rand(0.150 .. 0.200).round(3)
          #end_device_measurements[:mox_tgs880] = measurement
        
        elsif infra_parameter_name == "Smoke Voltage"
        
          measurement = rand(5.00 .. 6.00).round(2)
          #end_device_measurements[:smoke_voltage] = measurement
        
        elsif infra_parameter_name == "Ion Voltage"
          
          measurement = rand(0.00 .. 1.00).round(4)
          #end_device_measurements[:ion_voltage] = measurement
        
        elsif infra_parameter_name == "Electric Consumption"
        
          #measurement = rand(0.000034 .. 0.000036) * data_retrieve_period
          #end_device_measurements[:kwh] == measurement

          cached_measurement = $redis.get("simulated_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading")

          if cached_measurement.nil?
            measurement = 10.000000
          else
            measurement = cached_measurement.to_f + rand(0.000034 .. 0.000036) * data_retrieve_period
            max_measurement_allowed = measurement
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end
        end

        if measurement.present?
          end_device_measurement_map = {
            :name => infra_parameter_name,
            :measurement => measurement,
            :unit => infra_parameter_unit
          }

          end_device_measurements_array << end_device_measurement_map

          $redis.set("simulated_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading", measurement)
        end
      end

      end_device_map[:end_device_measurements] = end_device_measurements_array

      end_devices_array << end_device_map
    end

    simulated_data[:end_devices] = end_devices_array
    simulated_data
  end


  def generate_simulated_data_scenario_fire_occurs(opts = {})

    simulated_data = {}

    service_infra_scenario = opts[:service_infra_scenario]
    data_retrieve_period = opts[:data_retrieve_period]

    
    selected_house_section_id = 1#opts[:house_section_id].to_i
    end_devices = self.end_devices
    end_devices_array = []

    simulated_data[:house] = end_devices.first.try(:house).try(:as_json) || House.first.try(:as_json)
    simulated_data[:gateway_timestamp] = Time.now.to_i

    end_devices.each do |end_device|
      end_device_map = {}    

      end_device_map[:id] = end_device.id
      end_device_map[:name] = end_device.name
      end_device_map[:house_section] = end_device.try(:house_section).try(:as_json)
      
      #end_device_measurements = {}

      end_device_id = end_device.id
      end_device_house_section_id = end_device.house_section_id

      end_device_measurements_array = []


      end_device.infra_parameters.each do |infra_parameter|
        infra_parameter_name = infra_parameter.name
        infra_parameter_unit = infra_parameter.unit

        measurement = nil
        cached_measurement = $redis.get("simulated_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading")

        if infra_parameter_name == "Temperature"
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = rand(25.00 .. 30.00).round(2)
          else
            measurement = cached_measurement.to_f + ( ServiceInfra::SCENARIO_FIRE_TEMPERATURE_INCREASE_PER_SECOND * data_retrieve_period)
            max_measurement_allowed = 600.00
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed

            #$house_event_logger.info "\n measurement change - #{measurement}"
          end

        #  end_device_measurements[:temperature] = measurement

        elsif infra_parameter_name == "CO" 
         
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = measurement = rand(0.00 .. 2.00).to_f / 100.00
          else
            measurement = (cached_measurement.to_f * 100) + ( ServiceInfra::SCENARIO_FIRE_CO_INCREASE_PER_SECOND * data_retrieve_period)
            max_measurement_allowed = rand(300.00 .. 1000.00).round(2)
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

         # end_device_measurements[:co] = measurement

        elsif infra_parameter_name == "CO2"
          
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = measurement = rand(200 .. 500).to_f / 100.00
          else
            measurement = cached_measurement.to_i + ServiceInfra::SCENARIO_FIRE_CO_INCREASE_PER_SECOND * data_retrieve_period
            max_measurement_allowed = 400
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

          #end_device_measurements[:co2] = measurement

        elsif infra_parameter_name == "MOX GS822"
          
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = rand(0.70 .. 0.89).round(2)
          else
            measurement = cached_measurement.to_f + ServiceInfra::SCENARIO_FIRE_MOX_GS822_INCREASE_PER_SECOND * data_retrieve_period
            max_measurement_allowed = 0.89
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

          #end_device_measurements[:mox_gs822] = measurement
        
        elsif infra_parameter_name == "MOX TGS880"
        
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = rand(0.080 .. 0.149).round(3)
          else
            measurement = cached_measurement.to_f + ServiceInfra::SCENARIO_FIRE_MOX_TGS880_INCREASE_PER_SECOND * data_retrieve_period
            max_measurement_allowed = 0.149
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

          #end_device_measurements[:mox_tgs880] = measurement
        
        elsif infra_parameter_name == "Smoke Voltage"
        
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = rand(5.000 .. 6.000).round(3)
          else
            measurement = cached_measurement.to_f + ServiceInfra::SCENARIO_FIRE_SMOKE_VOLTAGE_INCREASE_PER_SECOND * data_retrieve_period
            max_measurement_allowed = 8.000
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

          #end_device_measurements[:smoke_voltage] = measurement
        
        elsif infra_parameter_name == "Ion Voltage"
          #cached_measurement = $redis.get("simulated_data_scenario_fire_occurs_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_reading")
          
          if cached_measurement.nil? || end_device_house_section_id != selected_house_section_id
            measurement = rand(0.100).round(2)
          else
            measurement = cached_measurement.to_f + ServiceInfra::SCENARIO_FIRE_ION_VOLTAGE_INCREASE_PER_SECOND * data_retrieve_period
            max_measurement_allowed = 0.99
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

          #end_device_measurements[:ion_voltage] = measurement

        elsif infra_parameter_name == "Electric Consumption"
          if cached_measurement.nil?
            measurement = 10.000000
          else
            measurement = cached_measurement.to_f + rand(0.000034 .. 0.000036) * data_retrieve_period
            max_measurement_allowed = measurement
            measurement = max_measurement_allowed if measurement >= max_measurement_allowed
          end

        end

        if measurement.present?
          end_device_measurement_map = {
            :name => infra_parameter_name,
            :measurement => measurement,
            :unit => infra_parameter_unit
          }

          end_device_measurements_array << end_device_measurement_map

          $redis.set("simulated_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading", measurement)
        end
      end

      end_device_map[:end_device_measurements] = end_device_measurements_array

      end_devices_array << end_device_map
    end

    simulated_data[:end_devices] = end_devices_array
    simulated_data
  end

end