class InfraParameter < ApplicationRecord
  include Actions::InfraParameter

  # => Constants ...
  INFRA_PARAMETER_RETRIEVE_TYPE_SUM = "sum"
  INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE = "average"
  INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE = "last_value"

  INFRA_PARAMETER_RETRIEVE_TYPES = [
    INFRA_PARAMETER_RETRIEVE_TYPE_SUM,
    INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE,
    INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE
  ]


  # => Relations ...
  has_many :end_device_info_infra_parameters, dependent: :destroy
  has_many :end_device_infos, :through => :end_device_info_infra_parameters


  # => Validations ...
  validates :name, presence: true
  validates :unit, presence: true
  validates :retrieve_type, inclusion: { :in => INFRA_PARAMETER_RETRIEVE_TYPES }

  validates_uniqueness_of :name, scope: :unit
  #validates :min_value, presence: true
  #validates :max_value, presence: true
  #validates :fractional_accuracy, presence: true, inclusion: { :in => 0..10 }

end
