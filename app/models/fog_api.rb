class FogApi

  EC2_BASE_URL = "http://ec2-13-232-14-215.ap-south-1.compute.amazonaws.com/"


  # => Accessors ...
  attr_accessor :conn, :errors

  def initialize(url)
    # => We initialize the connection instance with faraday framework ...
    # => We set the response type with application / json ...
    # => The form data is url encoded and we are using default net adapter using Net::HTTP ...

    @conn = Faraday.new(:url => url) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      faraday.headers['Content-Type'] = 'application/json'
    end 
    @errors = {}
  end

  def is_initiated?
    @conn.present?
  end
  
end