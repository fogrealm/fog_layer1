class EndDeviceInfo < ApplicationRecord
  include Actions::EndDeviceInfo
  
  # => Relations ...
  has_many :end_devices, dependent: :destroy
  has_many :end_device_info_infra_parameters, dependent: :destroy
  has_many :infra_parameters, :through => :end_device_info_infra_parameters


  # => Validations ...
  validates :name, presence: true
  validates :description, presence: true
  validates :model, presence: true
  validates :manufacturer, presence: true
  
end
