class FogMain
  include Actions::FogMain

  attr_accessor :fog_simulator, :service_task_manager

  def initialize(args)
    self.fog_simulator = nil
    self.service_task_manager = nil
  end

  def self.initiate_layer1_services

    fire_alarm_device_gateway = DeviceGateway.find(1)
    smart_meter_device_gateway = DeviceGateway.find(2)

    service_task_manager = ServiceTaskManager.new

    simulator = Simulator.new
    simulator.fetch_simulated_data(fire_alarm_device_gateway)
    simulator.fetch_simulated_data(smart_meter_device_gateway)
    #simulator.fetch_simulated_data(smart_meter_device_gateway)

    Rails.logger.info "\n After 1"
    
    service_task_manager.generate_service_tasks
    
    Rails.logger.info "\n After 2"

    service_task_manager.task_distribution_algorithm

    Rails.logger.info "\n After 3"

    FogMain.synchronize_data_periodically
    Rails.logger.info "\n Initiated Updated Management - Fog Layer 1 / 2"
  end

  class << self

    attr_accessor :data_sync_scheduler
  end
 
end