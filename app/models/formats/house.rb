module Formats::House

  def as_json(opts = {})
    {
      id: self.id,
      name: self.name,
      address: self.address,
      landmark: self.landmark, 
      description: self.description,
      location: self.location.as_json,
      icfn_id: self.icfn_id,
      icfn_base_url: self.icfn_base_url,
      icfn_channel_key: self.icfn_channel_key,
      cloud_id: self.cloud_id,
      cloud_base_url: self.cloud_base_url
    }
  end
end