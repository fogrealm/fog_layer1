class EndDevice < ApplicationRecord
  # include Formats::EndDevice
  #include Conditions::EndDevice
  include Actions::EndDevice

  
  # => Relations ...
  has_many :house_events
  has_many :infra_parameters, :through => :end_device_info 
  has_many :infra_readings
  #has_many :end_devices

  belongs_to :end_device_info
  belongs_to :house_section
  belongs_to :service_infra
  belongs_to :device_gateway


  # => Validations ...
  validates :name, presence: true
  validates :end_device_info, :house_section, :device_gateway, presence: true
  validates :should_alert_status, inclusion: { :in => [ true, false ] }
  validates :serial_number, presence: true
  
  validates_uniqueness_of :serial_number

  # => Delegations ...
  delegate :house, to: :house_section

  # useful for fast creation of end devices in admin panel ...
  # before_validation do
  #  self.name = "#{self.end_device_info.name} Device  #{self.house_section.name}"
  # end


end
