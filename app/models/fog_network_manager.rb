 class FogNetworkManager

  CLOUD_TEST_EVENT_ROUTE_PATH = "/house_events/test_event_controller.json"
  CLOUD_SEND_INFRA_READING_PATH = "/infra_readings.json"
  CLOUD_SEND_HOUSE_EVENT_PATH = "/house_events.json"

  CLOUD_BASE_URL = "http://0.0.0.0:5000"
  #CLOUD_BASE_URL = "http://ec2-13-233-93-161.ap-south-1.compute.amazonaws.com:5000"

  FOG_MAINTAIN_BASE_URL = "http://0.0.0.0:5050"
  #FOG_MAINTAIN_BASE_URL = "http://ec2-13-233-3-95.ap-south-1.compute.amazonaws.com:5050"

  RAILS_INSTANCES_BASE_URL = "http://0.0.0.0"

  RAILS_INSTANCES_MASTER_PORT = 4000
  #RAILS_INSTANCES_MASTER_PORT = 4001
  RAILS_INSTANCES_PORT_RANGE = 4001 .. 4003


  class << self

    def send_request_to_rails_controller(service_task_entity_map, rails_instance_port_no, controller_connections)
    
      network_operation_status = false
      
       
      max_retry_count = service_task_entity_map[:max_retry_count]
      retry_count = service_task_entity_map[:retry_count]

      while retry_count < max_retry_count

        opts = {}
        opts[:service_task_entity_map] = service_task_entity_map
        opts[:port_no] = rails_instance_port_no
        request_controller_spec = PlatformConfig.generate_request_controller_spec( opts )

        network_operation_status = true
    
        if request_controller_spec.present?

          route = request_controller_spec[:route]
          route_parameters = request_controller_spec[:route_parameters]

          puts "\n params -> #{route_parameters.to_json}"

            
          conn = controller_connections[rails_instance_port_no]
          response = conn.post route, route_parameters.to_json rescue network_operation_status = false
          
          if network_operation_status == false 
            priority = service_task_entity_map[:service_priority]
              
            retry_count = retry_count + 1
            service_task_entity_map[:retry_count] = retry_count + 1
            if rails_instance_port_no == FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.last
              rails_instance_port_no = FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.first
            else 
              rails_instance_port_no = rails_instance_port_no + 1
            end

            #service_task_pqueue.push service_task_entity_map, service_task_entity_map[:enqueued_timestamp]
          end
        else
          retry_count = retry_count + 1
          service_task_entity_map[:retry_count] = retry_count + 1
        end  
        
        return if network_operation_status == true
    
      end
      
      if network_operation_status == false
        ServiceTask.save_in_db!(service_task_entity_map)
        # => Write code to inform cloud as well ...

        FogError.create(house_id: 1, failed_error_port: rails_instance_port_no.to_s, fog_error_type: FogError::FOG_ERROR_TYPE_NODE_FAILURE, reported_at_timestamp: Time.now.to_i)
      end
    
    end

    
    def fetch_update_status
      # => This Performs an API call to fog maintain to check whether any update is required or not ...

      fog_api = FogApi.new(FOG_MAINTAIN_BASE_URL)
      current_house = House.first
      check_update_route = "/houses/#{current_house.id}/check_updates.json"
      
      response = fog_api.conn.get check_update_route, { ignore_security: true }
      puts "response - #{response.body}"

      response_body = JSON.parse(response.body)
      updation_status = response_body["updation_status"]

      status  = updation_status["is_recently_updated"]

      status
      #puts "\n updation status - #{updation_status}"
      
    end


    def synchronize_all_model_entities(fog_fetch_api = nil, fog_post_api = nil, current_house)
      # => This method will update all the model entities within the fog layer 1 ...
      # => The Data is fetched from fog maintain server ...
      # => Once the data within the layer 1 is synchronized ...
      # => Then, the corresponding layer 2 entities (if only needed to ) will be also synchronized ...

      EndDeviceInfo.fetch_and_update_end_device_infos_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      InfraParameter.fetch_and_update_infra_parameters_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      EndDeviceInfoInfraParameter.fetch_and_update_end_device_info_infra_parameters_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      House.fetch_and_update_house_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      HouseSection.fetch_and_update_house_sections_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      ServiceInfra.fetch_and_update_service_infras_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      DeviceGateway.fetch_and_update_device_gateways_from_fog_maintain(fog_fetch_api, current_house)
      EndDevice.fetch_and_update_end_devices_from_fog_maintain(fog_fetch_api, fog_post_api, current_house)
      

      House.inform_fog_master_icnf_base_url

    end

    
    def update_house_updation_status_in_fog_maintain(current_house)
      # => This method will update the status of the house as updated ...
    
      fog_updation_complete_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL)    
      set_house_update_route = "/houses/#{current_house.id}/set_house_recently_updated.json"

      fog_updation_complete_api.conn.post set_house_update_route, { ignore_security: true }.to_json
    end    

    handle_asynchronously :send_request_to_rails_controller
  end

end