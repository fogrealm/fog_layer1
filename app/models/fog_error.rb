class FogError < ApplicationRecord
  # => Constants ...

  FOG_ERROR_TYPE_NODE_FAILURE = "node_failure"

  FOG_ERROR_TYPES = [
    FOG_ERROR_TYPE_NODE_FAILURE
  ]


  # => Relations ...
  belongs_to :house


  # => Validations ...
  validates :house, presence: true
  validates :fog_error_type, inclusion: { :in => FOG_ERROR_TYPES }


  # => Scopes ...
  scope :error_type_node_failure, -> {
    where(
      fog_error_type: FOG_ERROR_TYPE_NODE_FAILURE
    )
  }


  # => Callbacks ...
  before_validation on: :create do
    if self.fog_error_type.nil?
      self.fog_error_type = FOG_ERROR_TYPE_NODE_FAILURE
    end

    if self.reported_at_timestamp.present?
      self.reported_at = self.reported_at_timestamp
    end
  end


  # => Accessors ...
  attr_accessor :reported_at_timestamp

end
