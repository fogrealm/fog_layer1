class ServiceTask < ApplicationRecord
  include Actions::ServiceTask

  # => Constants ...
  MIN_PRIORITY = 1
  MAX_PRIORITY = 8

  SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION = "end_device_information"
  SERVICE_TASK_TYPE_OTHERS = "others"

  SERVICE_TASK_TYPES = [
    SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION,
    SERVICE_TASK_TYPE_OTHERS
  ]

  END_DEVICE_DATA_INFORM_MAX_RETRY_COUNT = 3

  SERVICE_TASK_PRIORITY_RETRY_TIMESTAMP_DIFF_MAP = {
    1 => 5,
    2 => 10,
    3 => 15,
    4 => 20,
    5 => 25
  }
  
  # => Relations ...
  belongs_to :service_infra
  belongs_to :infra_reading
  
  has_one :service_task, dependent: :destroy


  # => Delegations ...
  delegate :infra_parameter, to: :infra_reading
  delegate :end_device, to: :infra_reading
  delegate :house_section, to: :end_device


  # => Validations ...
  validates :gateway_timestamp, presence: true
  validates :service_priority, presence: true, inclusion: { :in => MIN_PRIORITY .. MAX_PRIORITY }
  validates :service_task_type, presence: true, inclusion: { :in => SERVICE_TASK_TYPES } 


  # => Nested Attributes ...
  accepts_nested_attributes_for :infra_reading


  # => Accessors ...
  attr_accessor :service_task_entity_map

  # => Callbacks ...
  before_validation do
    # => Assigning service task type as others if nothing is mentioned ...

    if self.service_task_type.nil?
      self.service_task_type = SERVICE_TASK_TYPE_OTHERS
    end
  end

end
