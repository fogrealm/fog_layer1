class HouseSection < ApplicationRecord
  include Formats::HouseSection
  include Actions::HouseSection

  # => Relations ...
  belongs_to :house

  has_many :end_devices
  has_many :service_infras, :through => :end_devices, :source => :service_infra


  # => Validations ...
  validates :name, presence: true

end