class HouseEvent < ApplicationRecord

  # => Constants ...
  HOUSE_EVENT_TYPE_ALERT = "alert"
  HOUSE_EVENT_TYPE_INFORM = "inform"

  HOUSE_EVENT_TYPES = [
    HOUSE_EVENT_TYPE_ALERT,
    HOUSE_EVENT_TYPE_INFORM
  ]
  

  # => Relations ...
  belongs_to :house_section
  

  # => Validations ...
  validates :name, presence: true
  validates :house_event_type, inclusion: { :in => HOUSE_EVENT_TYPES }
  validates :event_level, inclusion: { :in => (1..10)}


  # => Callbacks ...
  before_validation on: :create do 
    # => If house event type is not mentioned, then we take type as 'inform' by default ...
    # => If event level is not mentioned, then we take default level as 1 ...

    if self.house_event_type.nil?
      self.house_event_type = HOUSE_EVENT_TYPE_INFORM
    end

    if self.event_level.nil?
      self.event_level = 1
    end
  end

  class << self

    # => Accessors ...
    # => These are schedulers defined inside the Class HouseEvent ...

    attr_accessor :fire_detection_scheduler, :smart_meter_scheduler,
                  :icfn_forwarding_scheduler # => This needs to be moved from here later. Not clean...

  end
end
