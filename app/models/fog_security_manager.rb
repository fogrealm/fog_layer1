class FogSecurityManager
  
  # => Constants ...
  DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME = "AES-192-CBC"
  KEY_SIZE_BYTES = 24

  
  class << self

    def symmetrically_encrypted_payload_data(current_house = nil, plain_payload_data_hash = {}, symmetric_encryption_scheme = nil)
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      symmetric_encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
      current_house ||= House.first

      cipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      cipher.encrypt

      authentication_token = current_house.authentication_token
      authentication_token_padded_24_bytes = authentication_token.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = authentication_token_padded_24_bytes

      plain_payload_data = plain_payload_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end


    def retrieve_plain_json_from_encrypted_json_list(encrypted_json_list = nil, current_house = nil)
      return if encrypted_json_list.nil? || current_house.nil?

      plain_json_list = []
      encrypted_json_list.each do |encrypted_json|
        plain_json_list << retrieve_plain_json_from_encrypted_json(encrypted_json, current_house)
      end

      plain_json_list
    end    
  
    
    def retrieve_plain_json_from_encrypted_json(encrypted_json, current_house, opts = {})
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The current house information is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      #encrypted_json = JSON.parse(encrypted_json_response.body)
      asymmetrically_encrypted_symmetric_encryption_info = Base64.decode64(encrypted_json["asymmetrically_encrypted_symmetric_encryption_info"])
      symmetrically_encrypted_payload_data = Base64.decode64(encrypted_json["symmetrically_encrypted_payload_data"])

      private_key_content = File.read("house_#{current_house.id}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      asymmetrically_encrypted_symmetric_encryption_info_json_string = private_key.private_decrypt(Base64.decode64(asymmetrically_encrypted_symmetric_encryption_info))

      asymmetrically_encrypted_symmetric_encryption_info_json = JSON.parse(asymmetrically_encrypted_symmetric_encryption_info_json_string)
      base64_encoded_symmetric_key = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_key"]
      base64_encoded_initial_vector = asymmetrically_encrypted_symmetric_encryption_info_json["initial_vector"]

      symmetric_encryption_scheme = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_encryption_scheme"]
      symmetric_key = Base64.decode64(base64_encoded_symmetric_key)
      initial_vector = Base64.decode64(base64_encoded_initial_vector)

      puts "\n encryption_info - #{symmetric_encryption_scheme}, #{symmetric_key}, #{initial_vector}"

      decipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      decipher.decrypt
      decipher.key = symmetric_key
      decipher.iv = initial_vector

      payload_data_json_string = decipher.update(symmetrically_encrypted_payload_data) + decipher.final
      plain_json = JSON.parse(payload_data_json_string)
      plain_json
    end


    def fog_request_encoded_token_signature(current_house)
      # => The following method generates the base64 token signature ...
      # => The message signed contains house_id and house_authentication_token ...

      raise "Invalid Current House" unless current_house.is_a?(House)

      private_key_content = File.read("house_#{current_house.id}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      plain_message = { house_id: current_house.id, house_authentication_token: current_house.authentication_token }.to_json            
      signature = private_key.sign(OpenSSL::Digest::SHA256.new, plain_message)
      Base64.encode64(signature)
    end


    def symmetrically_encrypt_plain_data_hash(plain_data_hash = {}, encryption_key = nil, encryption_scheme = nil, opts = {})
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The encryption key and encryption scheme are taken as function arguments ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
  
      cipher = OpenSSL::Cipher.new(encryption_scheme)
      cipher.encrypt

      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = encryption_key_padded_keysize_bytes

      plain_payload_data = plain_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end


    def plain_data_hash_from_symmetrically_encrypted_data(encrypted_data = nil, encryption_key = nil, encryption_scheme = nil, opts = {})
      # => The following function returns the plain data hash for the encrypted_data passed as argument ...
      # => The encrypted data is base64 encoded json string data ...
      # => The other arguments passed include encryption_key and encryption_scheme

      raise "Invalid encrypted data" if encrypted_data.nil?
      
      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME

      raise "Invalid encryption key" if encryption_key.nil?

      base64_decoded_symmetrically_encrypted_data = Base64.decode64(encrypted_data)
      
      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')

      decipher = OpenSSL::Cipher.new(encryption_scheme)
      decipher.decrypt
      decipher.key = encryption_key_padded_keysize_bytes
    
      payload_data_json_string = decipher.update(base64_decoded_symmetrically_encrypted_data) + decipher.final
      payload_data_hash = JSON.parse(payload_data_json_string)
      payload_data_hash
    end


    def retrieve_most_trustworthy_port(port_numbers = nil)
      # => We will select the port with least failure experience out of the last 10 failure events ...

      port_numbers ||= FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.to_a
      failed_ports = FogError.error_type_node_failure.map(&:failed_port).compact.map(&:to_i)

      unfailed_ports = port_numbers - failed_ports.uniq
      return unfailed_ports.first if unfailed_ports.any?

      port_frequency_hash = {}
      failed_ports.each do |failed_port|
        port_frequency_hash[failed_port] = 0 if port_frequency_hash[failed_port].nil?
        port_frequency_hash[failed_port] += 1
      end

      failed_ports.min_by{|key| port_frequency_hash[key]}
    end


  end

end