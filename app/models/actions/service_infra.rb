module Actions::ServiceInfra
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_service_infras_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil,  current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?

    current_house = House.first if current_house.nil?
    fetch_service_infras_route = "/houses/#{current_house.id}/service_infras.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_service_infras_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    service_infras_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house) rescue nil

    puts "\n plain response json - #{service_infras_json}"

    if service_infras_json.present?

      current_existing_ids = service_infras_json.map{|service_infra_json| service_infra_json["id"]}
      service_infras_to_be_removed = ServiceInfra.where.not(id: current_existing_ids)
      service_infras_to_be_removed.destroy_all

      service_infras_json.each do |service_infra_json|
        id = service_infra_json["id"]
        name = service_infra_json["name"]
        priority = service_infra_json["priority"]
        house_id = current_house.id
        
        service_infra = ServiceInfra.where(id: id).first

        if service_infra.nil?
          service_infra = ServiceInfra.new
        end

        service_infra.id = id
        service_infra.name = name
        service_infra.priority = priority
        service_infra.house_id = house_id
        
        service_infra.save!
      end

      synchronize_data_route = "/houses/#{current_house.id}/service_infras/synchronize_data.json"
      payload = {}
      payload[:service_infras_json] = service_infras_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json
    end
  end

  handle_asynchronously :fetch_and_update_service_infras_from_fog_maintain

end


module InstanceMethods

end