module Actions::EndDeviceInfo
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_end_device_infos_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?

    current_house = House.first if current_house.nil?
    fetch_end_device_infos_route = "/end_device_infos.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_end_device_infos_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    end_device_infos_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house) rescue nil

    puts "\n plain response json - #{end_device_infos_json}"

    if end_device_infos_json.present?

      current_existing_ids = end_device_infos_json.map{|end_device_info_json| end_device_info_json["id"]}
      end_device_infos_to_be_removed = EndDeviceInfo.where.not(id: current_existing_ids)
      end_device_infos_to_be_removed.destroy_all

      end_device_infos_json.each do |end_device_info_json|
        id = end_device_info_json["id"]
        name = end_device_info_json["name"]
        description = end_device_info_json["description"]
        model = end_device_info_json["model"]
        manufacturer = end_device_info_json["manufacturer"]

        end_device_info = EndDeviceInfo.where(id: id).first

        if end_device_info.nil?
          end_device_info = EndDeviceInfo.new
        end

        end_device_info.id = id
        end_device_info.name = name
        end_device_info.description = description
        end_device_info.model = model
        end_device_info.manufacturer = manufacturer

        end_device_info.save!
      end

      synchronize_data_route = "/end_device_infos/synchronize_data.json"
      payload = {}
      payload[:end_device_infos_json] = end_device_infos_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json
    end


  end

  handle_asynchronously :fetch_and_update_end_device_infos_from_fog_maintain
  
end


module InstanceMethods

end