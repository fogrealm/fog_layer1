module Actions::EndDevice
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_end_devices_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?


    current_house = House.first if current_house.nil?
    fetch_end_devices_route = "/houses/#{current_house.id}/end_devices.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_end_devices_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    end_devices_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house) rescue nil

    puts "\n plain response json - #{end_devices_json}"

    if end_devices_json.present?

      current_existing_ids = end_devices_json.map{|end_device_json| end_device_json["id"]}
      end_devices_to_be_removed = EndDevice.where.not(id: current_existing_ids)
      end_devices_to_be_removed.destroy_all

      end_devices_json.each do |end_device_json|
        id = end_device_json["id"]
        name = end_device_json["name"]
        house_section_id = end_device_json["house_section_id"]
        end_device_info_id = end_device_json["end_device_info_id"]
        service_infra_id = end_device_json["service_infra_id"]
        device_gateway_id = end_device_json["device_gateway_id"]
        should_alert_status = end_device_json["should_alert_status"]
        serial_number = end_device_json["serial_number"]

        end_device = EndDevice.where(id: id).first

        if end_device.nil?
          end_device = EndDevice.new
        end

        end_device.id = id
        end_device.name = name
        end_device.house_section_id = house_section_id
        end_device.end_device_info_id = end_device_info_id
        end_device.service_infra_id = service_infra_id
        end_device.device_gateway_id = device_gateway_id
        end_device.should_alert_status = should_alert_status
        end_device.serial_number = serial_number

        end_device.save!
      end

      synchronize_data_route = "/houses/#{current_house.id}/end_devices/synchronize_data.json"
      payload = {}
      payload[:end_devices_json] = end_devices_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json

    end
  end

  handle_asynchronously :fetch_and_update_end_devices_from_fog_maintain
  
end


module InstanceMethods

end