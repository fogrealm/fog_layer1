module Actions::HouseSection
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_house_sections_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?


    current_house = House.first if current_house.nil?
    fetch_house_sections_route = "/houses/#{current_house.id}/house_sections.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_house_sections_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json response list - #{encrypted_json_list_response.body}"
    
    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    house_sections_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house)

    puts "\n plain response json - #{house_sections_json}"


    if house_sections_json.present?
      
      current_existing_ids = house_sections_json.map{|house_section_json| house_section_json["id"]}
      house_sections_to_be_removed = HouseSection.where.not(id: current_existing_ids)
      house_sections_to_be_removed.destroy_all


      house_sections_json.each do |house_section_json|
        id = house_section_json["id"]
        name = house_section_json["name"]
        house_id = house_section_json["house_id"]

        house_section = HouseSection.where(id: id).first

        if house_section.nil?
          house_section = HouseSection.new
        end
      
        house_section.id = id
        house_section.name = name
        house_section.house_id = house_id
        house_section.save!
      
      end

      synchronize_data_route = "/houses/#{current_house.id}/house_sections/synchronize_data.json"
      payload = {}
      payload[:house_sections_json] = house_sections_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json
    end

  end

  handle_asynchronously :fetch_and_update_house_sections_from_fog_maintain
  
end


module InstanceMethods

end