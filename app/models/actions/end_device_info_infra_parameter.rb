module Actions::EndDeviceInfoInfraParameter
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_end_device_info_infra_parameters_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?

    current_house = House.first if current_house.nil?
    fetch_end_device_info_infra_parameters_route = "/end_device_info_infra_parameters.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_end_device_info_infra_parameters_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"

    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    end_device_info_infra_parameters_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house) rescue nil

    puts "\n plain response json - #{end_device_info_infra_parameters_json}"

    if end_device_info_infra_parameters_json.present?

      current_existing_ids = end_device_info_infra_parameters_json.map{|end_device_info_infra_parameter_json| end_device_info_infra_parameter_json["id"]}
      end_device_info_infra_parameters_to_be_removed = EndDeviceInfoInfraParameter.where.not(id: current_existing_ids)
      end_device_info_infra_parameters_to_be_removed.destroy_all

      end_device_info_infra_parameters_json.each do |end_device_info_infra_parameter_json|
        id = end_device_info_infra_parameter_json["id"]
        end_device_info_id = end_device_info_infra_parameter_json["end_device_info_id"]
        infra_parameter_id = end_device_info_infra_parameter_json["infra_parameter_id"]
        
        end_device_info_infra_parameter = EndDeviceInfoInfraParameter.where(id: id).first

        if end_device_info_infra_parameter.nil?
          end_device_info_infra_parameter = EndDeviceInfoInfraParameter.new
        end

        end_device_info_infra_parameter.id = id
        end_device_info_infra_parameter.end_device_info_id = end_device_info_id
        end_device_info_infra_parameter.infra_parameter_id = infra_parameter_id
        
        end_device_info_infra_parameter.save!
      end

      synchronize_data_route = "/end_device_info_infra_parameters/synchronize_data.json"
      payload = {}
      payload[:end_device_info_infra_parameters_json] = end_device_info_infra_parameters_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json

    end
  end

  handle_asynchronously :fetch_and_update_end_device_info_infra_parameters_from_fog_maintain
  
end


module InstanceMethods

end