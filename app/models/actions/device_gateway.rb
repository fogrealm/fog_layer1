module Actions::DeviceGateway
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def fetch_and_update_device_gateways_from_fog_maintain( fog_fetch_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?

    current_house = House.first if current_house.nil?
    fetch_device_gateways_route = "/houses/#{current_house.id}/device_gateways.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_list_response = fog_fetch_api.conn.get fetch_device_gateways_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json list response - #{encrypted_json_list_response.body}"
    
    encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
    device_gateways_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list, current_house) rescue nil

    puts "\n plain response json - #{device_gateways_json}"

    if device_gateways_json.present?

      current_existing_ids = device_gateways_json.map{|device_gateway_json| device_gateway_json["id"]}
      device_gateways_to_be_removed = DeviceGateway.where.not(id: current_existing_ids)
      device_gateways_to_be_removed.destroy_all

      device_gateways_json.each do |device_gateway_json|
        id = device_gateway_json["id"]
        name = device_gateway_json["name"]
        device_gateway_url = device_gateway_json["device_gateway_url"]
        base_url = device_gateway_json["base_url"]
        channel_key = device_gateway_json["channel_key"]
        channel_encryption_scheme = device_gateway_json["channel_encryption_scheme"]
        
        device_gateway = DeviceGateway.where(id: id).first

        if device_gateway.nil?
          device_gateway = DeviceGateway.new
        end

        device_gateway.id = id
        device_gateway.name = name
        device_gateway.device_gateway_url = device_gateway_url
        device_gateway.base_url = base_url
        device_gateway.channel_key = channel_key
        device_gateway.channel_encryption_scheme = channel_encryption_scheme

        device_gateway.save!
      end
    end
  end

  handle_asynchronously :fetch_and_update_device_gateways_from_fog_maintain
  
end


module InstanceMethods

end