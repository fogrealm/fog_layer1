module Actions::House
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module InstanceMethods
  
  def reset_authentication_token
    # => This method reset the authentication token to a new one ...

    self.authentication_token = generate_new_auth_token
    self.save!
  end

  def generate_new_auth_token
    # => This method generate a new authentication token ...
    
    token = Devise.friendly_token
    while House.where(authentication_token: token).count > 0
      token = Devise.friendly_token
    end
    token
  end

end


module ClassMethods

  def fetch_and_update_house_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil, current_house = nil )
    # => This method to fetch the current house from server and update this in the fog ...

    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?

    current_house = House.first if current_house.nil?
    fetch_house_route = "/houses/#{current_house.id}.json"

    encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature(current_house)
    plain_payload_data_hash = {
      encoded_token_signature: encoded_token_signature,
      house_id: current_house.id,
      house_authentication_token: current_house.authentication_token
    }
    encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(current_house, plain_payload_data_hash, nil)

    encrypted_json_response = fog_fetch_api.conn.get fetch_house_route, { encrypted_request_payload: encrypted_request_payload, layer: 1, house_id: current_house.id }
    puts "encrypted json response - #{encrypted_json_response.body}"
    
    encrypted_json = JSON.parse(encrypted_json_response.body)
    current_house_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json(encrypted_json, current_house) rescue nil

    puts "\n plain response json - #{current_house_json}"

    if current_house_json.present?
      id = current_house_json["id"]
      name = current_house_json["name"]
      address = current_house_json["address"]
      landmark = current_house_json["description"]

      location_id = current_house_json["location"]["id"]
      location_latitude = current_house_json["location"]["latitude"]
      location_longitude = current_house_json["location"]["longitude"]
      authentication_token = current_house_json["authentication_token"]

      icfn_id = icfn_base_url = cloud_id = cloud_base_url = nil
      icfn_channel_key = cloud_channel_key = nil

      nearby_icfn_node_json = current_house_json["nearby_icfn_node"]
      if nearby_icfn_node_json.present?
        icfn_id = current_house_json["nearby_icfn_node"]["icfn_id"]
        icfn_base_url = current_house_json["nearby_icfn_node"]["icfn_base_url"]
        icfn_channel_key = current_house_json["nearby_icfn_node"]["icfn_channel_key"]
      end

      nearby_cloud_server_json = current_house_json["nearby_cloud_server"]
      if nearby_cloud_server_json.present?
        cloud_id = current_house_json["nearby_cloud_server"]["cloud_id"]
        cloud_base_url = current_house_json["nearby_cloud_server"]["cloud_base_url"]
        cloud_channel_key = current_house_json["nearby_cloud_server"]["cloud_channel_key"]
      end

      house = House.where(id: id).first

      if house.present?
        house.id = id
        house.name = name
        house.address = address
        house.landmark = landmark
        house.authentication_token = authentication_token

        house.location.id = location_id
        house.location.latitude = location_latitude
        house.location.longitude = location_longitude

        house.icfn_id = icfn_id
        house.icfn_base_url = icfn_base_url
        house.icfn_channel_key = icfn_channel_key

        house.cloud_id = cloud_id
        house.cloud_base_url = cloud_base_url
        house.cloud_channel_key = cloud_channel_key

        house.save!
      else
        house = House.new
        house.id = id
        house.name = name
        house.address = address
        house.landmark = landmark
        house.authentication_token = authentication_token

        location = house.build_location
        location.id = location_id
        location.latitude = location_latitude
        location.longitude = location_longitude
        
        house.icfn_id = icfn_id
        house.icfn_base_url = icfn_base_url
        house.cloud_id = cloud_id
        house.cloud_base_url = cloud_base_url

        house.save!
      end

      synchronize_data_route = "/houses/#{current_house.id}/synchronize_data.json"
      payload = {}
      payload[:current_house_json] = current_house_json      
      fog_post_api.conn.post synchronize_data_route, payload.to_json

    end
  end


  def inform_fog_master_icnf_base_url(fog_fetch_api = nil, fog_post_api = nil, current_house = nil)
    fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
    fog_post_api = FogApi.new("#{FogNetworkManager::RAILS_INSTANCES_BASE_URL}:#{FogNetworkManager::RAILS_INSTANCES_MASTER_PORT}") if fog_post_api.nil?
    
    current_house = House.first if current_house.nil?
    fetch_nearby_icfn_node_route = "/icfn_nodes/nearby_icfn_node.json"

    payload = {}
    payload[:house_latitude] = current_house.location.latitude
    payload[:house_longitude] = current_house.location.longitude

    response = fog_fetch_api.conn.get fetch_nearby_icfn_node_route, payload
    puts "response - #{response.body}"

    nearby_icfn_node_json = JSON.parse(response.body)
    
    puts "\n response body - #{nearby_icfn_node_json}"

    if nearby_icfn_node_json.present?
      if nearby_icfn_node_json["icfn_node"].present? && nearby_icfn_node_json["icfn_node"]["id"].present?
        nearby_icfn_node_base_url = nearby_icfn_node_json["icfn_node"]["base_url"]

        if nearby_icfn_node_base_url.present?
          payload = {}
          update_icfn_base_url_route = "/houses/#{current_house.id}/update_icfn_base_url.json"
          payload[:icnf_base_url] = nearby_icfn_node_base_url      
          fog_post_api.conn.post update_icfn_base_url_route, payload.to_json
        end
      end
    end
  end

  handle_asynchronously :fetch_and_update_house_from_fog_maintain
  handle_asynchronously :inform_fog_master_icnf_base_url
    
end


module InstanceMethods

end