module Actions::ServiceTask
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def save_in_db!(service_task_entity_map)
    # => Saving the entity in DB using the service_task_entity_map ...
    # => This method needs to be called in fog server when there is a need to ...
    
    service_task_entity_save_map = {}
    
    service_task_entity_save_map[:gateway_timestamp] = service_task_entity_map[:gateway_timestamp]
    service_task_entity_save_map[:service_priority] = service_task_entity_map[:service_priority]
    service_task_entity_save_map[:service_infra_id] = service_task_entity_map[:service_infra][:id]

    service_task_entity_save_map[:infra_reading_attributes] = {
      :infra_parameter_id => service_task_entity_map[:infra_reading][:infra_parameter][:id],
      :value => service_task_entity_map[:infra_reading][:value],
      :end_device_id => service_task_entity_map[:infra_reading][:end_device][:id]
    }

    service_task_entity_save_map[:retry_count] = service_task_entity_map[:retry_count]
    service_task_entity_save_map[:max_retry_count] = service_task_entity_map[:max_retry_count]
    service_task_entity_save_map[:service_task_type] = service_task_entity_map[:service_task_type]    

    ServiceTask.create!(service_task_entity_save_map)
  end
  
end


module InstanceMethods

end