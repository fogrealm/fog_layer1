module Actions::FogMain
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end


module ClassMethods

  def synchronize_data_periodically
    # => Method which will check for data content based updates from the fog maintain server ..

    return if House.count < 1

    FogMain.data_sync_scheduler = Rufus::Scheduler.new if FogMain.data_sync_scheduler.nil?

    current_house = House.first
    
    FogMain.data_sync_scheduler.every '30s' do
      
      update_status = FogNetworkManager.fetch_update_status
    
      if update_status == false 
        # => Need to look for a appraoch later so that i can store the fog_fetch_api and fog_post_api in a variable ...
        # => Currently can't do since it is leading to a delay job deserialization problem ...

        FogNetworkManager.synchronize_all_model_entities(nil, nil, current_house)
        FogNetworkManager.update_house_updation_status_in_fog_maintain(current_house)
      end
    
    end

  end
  
end

module InstanceMethods

  

end