class House < ApplicationRecord
  include ActiveRecordBaseCommon::Validations
  include Formats::House
  include Actions::House

  # => Relations ...
  #has_many :users, :dependent => :destroy
  has_many :admin_users, :dependent => :destroy
  has_many :house_sections, :dependent => :destroy
  #may have to throw this later on..
  #has_many :service_infras, :through => :house_service_infras
  has_many :service_infras, :dependent => :destroy
  has_many :end_devices, :through => :service_infras

  has_one :location, as: :locatable, :dependent => :destroy
  

  # => Validations ...
  validates :name, :address, :landmark, presence: true
  validates :location, has_one: true
  

  # => Associations ...
  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :house_sections, :allow_destroy => true


end
