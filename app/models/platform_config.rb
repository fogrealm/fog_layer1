class PlatformConfig < ApplicationRecord

  def self.generate_request_controller_spec( opts = {} )

    port_no = opts[:port_no]
    service_task_entity_map = opts[:service_task_entity_map]

    base_url = "http://localhost:#{port_no}"
    route = ""

    if service_task_entity_map[:service_task_type] = ServiceTask::SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION
      
      route_parameters = {}

      # => Service ID 1 is Fire Alarm System ...
      # => Service ID 2 is Smart Meter System ...
      # => We are hardcoding these values coz Users are not to allow these in Fog Admin Panel ...

      if service_task_entity_map[:service_infra][:id].to_i == 1
        route = ServiceInfra::SERVICE_INFRA_ROUTE_FIRE_ALARM_CONTROLLER
      elsif service_task_entity_map[:service_infra][:id] == 2
        route = ServiceInfra::SERVICE_INFRA_ROUTE_SMART_METER_CONTROLLER       
      else
        return nil
      end

      route_parameters[:service_task_entity_map] = service_task_entity_map.to_h
    end

    request_controller_spec = {
      port_no: port_no,
      base_url: base_url,
      route: route,
      full_url: base_url + route,
      route_parameters: route_parameters
    }

    request_controller_spec
  end
  
end
