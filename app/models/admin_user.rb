class AdminUser < ApplicationRecord
  include Formats::AdminUser

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  # => Relations ...
  belongs_to :house

  # => Validations ...
  validates :name, :house, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  
  validates_uniqueness_of :phone_number


  # => Callbacks ...
  before_validation do 

    # => Get the current house and update it ...
    self.house = House.first
    
  end


  before_validation on: :create do
    # => We set a random string value to unique identifier ...
    
    if self.unique_identifier.nil?
      self.unique_identifier = Devise.friendly_token
    end
  end



end
