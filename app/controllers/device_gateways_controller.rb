class DeviceGatewaysController < InheritedResources::Base
  respond_to :json

  def generate_simulated_data
    @device_gateway = DeviceGateway.find(params[:id].to_i)
    
    opts = {
      :service_infra_scenario => params[:service_infra_scenario].to_s,
      :data_retrieve_period => params[:data_retrieve_period].to_i
    }

    service_infra_scenario = params[:service_infra_scenario]
    data_retrieve_period = params[:data_retrieve_period]

    if data_retrieve_period.present? && ServiceInfra::SERVICE_INFRA_SCENARIOS.include?(service_infra_scenario)
      simulated_data = @device_gateway.generate_simulated_data(opts)
      
      encrypted_simulated_data = FogSecurityManager.symmetrically_encrypt_plain_data_hash(simulated_data, @device_gateway.channel_key, @device_gateway.channel_encryption_scheme, nil)
      
      render json: {
        :status => 200,
        :encrypted_simulated_data => encrypted_simulated_data
      } and return
    else 
      render json: { 
        error: "Invalid parameters send to server", 
        status: 422 
      } and return
    end

  end

end
