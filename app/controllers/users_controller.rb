class UsersController < InheritedResources::Base
  load_and_authorize_resource
  respond_to :json

  private

  def permitted_params
    params.permit(user: [:id, :email, :password, :password_confirmation, :current_password, :phone_number,
      profile_attributes: [:id, :first_name, :last_name, :dob, :dob_day, :dob_month, :dob_year, image_attributes: [:id, :file]]])
  end

end
