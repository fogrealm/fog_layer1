# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190408014259) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "name"
    t.string   "phone_number"
    t.integer  "house_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "unique_identifier"
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["house_id"], name: "index_admin_users_on_house_id", using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
  end

  create_table "audit_logs", force: :cascade do |t|
    t.string   "event"
    t.string   "message"
    t.integer  "auditor_id"
    t.string   "auditor_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "device_gateways", force: :cascade do |t|
    t.string   "name"
    t.string   "device_gateway_url"
    t.integer  "device_gateway_id"
    t.string   "channel_key"
    t.string   "channel_encryption_scheme"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "base_url"
    t.index ["device_gateway_id"], name: "index_device_gateways_on_device_gateway_id", using: :btree
  end

  create_table "end_device_info_infra_parameters", force: :cascade do |t|
    t.integer  "end_device_info_id"
    t.integer  "infra_parameter_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["end_device_info_id"], name: "index_end_device_info_infra_parameters_on_end_device_info_id", using: :btree
    t.index ["infra_parameter_id"], name: "index_end_device_info_infra_parameters_on_infra_parameter_id", using: :btree
  end

  create_table "end_device_infos", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "model"
    t.string   "manufacturer"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "end_devices", force: :cascade do |t|
    t.integer  "house_section_id"
    t.integer  "service_infra_id"
    t.integer  "end_device_info_id"
    t.integer  "device_gateway_id"
    t.string   "name"
    t.boolean  "should_alert_status", default: true
    t.string   "serial_number"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["device_gateway_id"], name: "index_end_devices_on_device_gateway_id", using: :btree
    t.index ["end_device_info_id"], name: "index_end_devices_on_end_device_info_id", using: :btree
    t.index ["house_section_id"], name: "index_end_devices_on_house_section_id", using: :btree
    t.index ["service_infra_id"], name: "index_end_devices_on_service_infra_id", using: :btree
  end

  create_table "fog_errors", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "fog_error_type", default: "node_failure"
    t.string   "failed_port"
    t.datetime "reported_at"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["failed_port"], name: "index_fog_errors_on_failed_port", using: :btree
    t.index ["fog_error_type"], name: "index_fog_errors_on_fog_error_type", using: :btree
    t.index ["house_id"], name: "index_fog_errors_on_house_id", using: :btree
  end

  create_table "house_events", force: :cascade do |t|
    t.integer  "house_section_id"
    t.string   "name"
    t.string   "house_event_type", default: "inform"
    t.integer  "event_level",      default: 1
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["house_section_id"], name: "index_house_events_on_house_section_id", using: :btree
  end

  create_table "house_sections", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["house_id"], name: "index_house_sections_on_house_id", using: :btree
  end

  create_table "house_security_templates", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "houses", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "landmark"
    t.text     "description"
    t.string   "authentication_token"
    t.integer  "icfn_id"
    t.string   "icfn_base_url"
    t.string   "icfn_channel_key"
    t.integer  "cloud_id"
    t.string   "cloud_base_url"
    t.string   "cloud_channel_key"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "infra_parameters", force: :cascade do |t|
    t.string   "name"
    t.string   "unit"
    t.string   "retrieve_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "unit"], name: "index_infra_parameters_on_name_and_unit", unique: true, using: :btree
    t.index ["retrieve_type"], name: "index_infra_parameters_on_retrieve_type", using: :btree
  end

  create_table "infra_readings", force: :cascade do |t|
    t.integer  "infra_parameter_id"
    t.integer  "end_device_id"
    t.float    "value"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["end_device_id"], name: "index_infra_readings_on_end_device_id", using: :btree
    t.index ["infra_parameter_id"], name: "index_infra_readings_on_infra_parameter_id", using: :btree
  end

  create_table "locations", force: :cascade do |t|
    t.string   "locatable_type"
    t.integer  "locatable_id"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "accuracy"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "platform_configs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "first_name", default: ""
    t.string   "last_name",  default: ""
    t.date     "dob"
    t.integer  "image_id"
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["first_name"], name: "index_profiles_on_first_name", using: :btree
    t.index ["last_name"], name: "index_profiles_on_last_name", using: :btree
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "service_infras", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "name"
    t.integer  "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["house_id"], name: "index_service_infras_on_house_id", using: :btree
    t.index ["name", "house_id"], name: "index_service_infras_on_name_and_house_id", unique: true, using: :btree
    t.index ["name"], name: "index_service_infras_on_name", using: :btree
    t.index ["priority"], name: "index_service_infras_on_priority", using: :btree
  end

  create_table "service_tasks", force: :cascade do |t|
    t.integer  "service_infra_id"
    t.integer  "infra_reading_id"
    t.integer  "service_priority",  default: 1
    t.integer  "gateway_timestamp"
    t.integer  "retry_count"
    t.integer  "max_retry_count"
    t.boolean  "is_drop_allowed",   default: false
    t.string   "service_task_type", default: "others"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["infra_reading_id"], name: "index_service_tasks_on_infra_reading_id", using: :btree
    t.index ["max_retry_count"], name: "index_service_tasks_on_max_retry_count", using: :btree
    t.index ["retry_count"], name: "index_service_tasks_on_retry_count", using: :btree
    t.index ["service_infra_id"], name: "index_service_tasks_on_service_infra_id", using: :btree
    t.index ["service_priority"], name: "index_service_tasks_on_service_priority", using: :btree
    t.index ["service_task_type"], name: "index_service_tasks_on_service_task_type", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "is_suspended",           default: false
    t.string   "phone_number",                           null: false
    t.string   "authentication_token",                   null: false
    t.integer  "profile_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["phone_number"], name: "index_users_on_phone_number", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
