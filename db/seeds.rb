# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Rails.env.development?
  house = House.new
  house.id = 1
  house.name = "Dragon House"
  house.address = "Dragon Ville, Dragon Street"
  house.landmark = "Near Pheonix House"
  house.description = "2 km from Dragon Bus Terminal"
  house.authentication_token = "LWeVfBsTSe8N1qum6Xm5"

  house.icfn_id = 1
  house.icfn_base_url = "http://0.0.0.0:3050"
  house.icfn_channel_key = "c2mC_zpvjcYphBCrEay2"

  house.cloud_id = 1
  house.cloud_base_url = "http://0.0.0.0:5000"
  house.cloud_channel_key = "59JV1T89Tmwn1UM5KVv6"

  location = house.build_location
  location.latitude, location.longitude = 10.7598267, 78.81599
  house.save!


  admin_user = AdminUser.new
  admin_user.email = "arjunthedragon@gmail.com"
  admin_user.password = "qwerty123"
  admin_user.password_confirmation = "qwerty123"
  admin_user.name = "Arjun K P"
  admin_user.phone_number = "9072263675"
  admin_user.save!

  DeviceGateway.create(
    [
      {
        "id"=>1, 
        "name"=>"Fire Alarm Gateway", 
        "base_url"=>"http://localhost:3000",
        "channel_key"=>"v8Gx76BSUx2WqndKzSV_",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      }, 
      {
        "id"=>2, 
        "name"=>"Smart Meter Gateway", 
        "base_url"=>"http://localhost:3000",
        "channel_key"=>"yZaG3WiCCg4RqkgcLCqh",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      }
    ]
  )

  HouseSection.create(
    [
      {
        :id=>1, 
        :name=>"Kitchen", 
        :house_id=>1
      }, 
      {
        :id=>2, 
        :name=>"Living Room", 
        :house_id=>1
      }, 
      {
        :id=>3, 
        :name=>"Bedroom", 
        :house_id=>1
      }
    ]
  )


  EndDeviceInfo.create(
    [
      {
        "id"=>1, 
        "name"=>"CO Sensor", 
        "description"=>"It reads the CO Level in PPM", 
        "model"=>"008809809", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>2, 
        "name"=>"CO2 Sensor", 
        "description"=>"It reads the CO2 Level in PPM", 
        "model"=>"0088099000", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"GS822", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>4, 
        "name"=>"Temperature Sensor", 
        "description"=>"To read Temerature in Celcius", 
        "model"=>"CEL8777897", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880 Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"89789797798", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>6, 
        "name"=>"Ion Voltage Sensor", 
        "description"=>"Sensor to read Ion votage in Gas", 
        "model"=>"ION8088909", 
        "manufacturer"=>"Woldra"
      }, 
      {
        "id"=>7, 
        "name"=>"Smart Meter Sensor", 
        "description"=>"Device to Read Electric Consumption in kwh.", 
        "model"=>"SMT900889", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>8, 
        "name"=>"Smoke Voltage Sensor", 
        "description"=>"Reads Smoke Voltage in the Gas", 
        "model"=>"098977888798", 
        "manufacturer"=>"Woldra"
      }
    ]
  )

  
  ServiceInfra.create(
    [
      {
        "id"=>1, 
        "house_id"=>1, 
        "name"=>"Fire Alarm Infrastructure", 
        "priority"=>1
      }, 
      {
        "id"=>2, 
        "house_id"=>1, 
        "name"=>"Smart Meter Infrastructure", 
        "priority"=>5
      }
    ]
  )


  InfraParameter.create(
    [
      {
        "id"=>2, 
        "name"=>"CO", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>3, 
        "name"=>"CO2", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>4, 
        "name"=>"MOX GS822", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>1, 
        "name"=>"Temperature", 
        "unit"=>"celcius",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>6, 
        "name"=>"Smoke Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>7, 
        "name"=>"Ion Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>8, 
        "name"=>"Electric Consumption", 
        "unit"=>"kwh",
        "retrieve_type"=>"last_value"
      }
    ]
  )

  
  EndDevice.create(
    [
      {
        "id"=>1, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"hhjhjkhjkhhj", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>2, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"kjgfghffd", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>3, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"wrwrwerwerwf", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>4, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>5, 
        "name"=>"MOX TGS880 Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"9898009", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>5, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"lkjjkhjfhgdfd", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>6, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>6, 
        "name"=>"Ion Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"cxhffjfjghg", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>7, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>8, 
        "name"=>"Smoke Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"qwrwrewewere", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>8, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"dss87766787786", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>9, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"khhgjgjghg", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>11, 
        "house_section_id"=>3, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Bedroom", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjkhjkhkjhjh", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>10, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjhhkjhj", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>12, 
        "house_section_id"=>2, 
        "service_infra_id"=>2, 
        "end_device_info_id"=>7, 
        "name"=>"Smart Meter Sensor Device", 
        "should_alert_status"=>true, 
        "serial_number"=>"SMhgfhghgffff", 
        "device_gateway_id"=>2
      }
    ]
  )

  EndDeviceInfoInfraParameter.create(
    [
      {
        "id"=>1, 
        "end_device_info_id"=>1, 
        "infra_parameter_id"=>2
      }, 
      {
        "id"=>2, 
        "end_device_info_id"=>2, 
        "infra_parameter_id"=>3
      }, 
      {
        "id"=>3, 
        "end_device_info_id"=>3, 
        "infra_parameter_id"=>4
      }, 
      {
        "id"=>4, 
        "end_device_info_id"=>4, 
        "infra_parameter_id"=>1
      }, 
      {
        "id"=>5, 
        "end_device_info_id"=>5, 
        "infra_parameter_id"=>5
      }, 
      {
        "id"=>6, 
        "end_device_info_id"=>6, 
        "infra_parameter_id"=>7
      }, 
      {
        "id"=>7, 
        "end_device_info_id"=>7, 
        "infra_parameter_id"=>8
      }, 
      {
        "id"=>8, 
        "end_device_info_id"=>8, 
        "infra_parameter_id"=>6
      }
    ] 
  )


  FogError.create(
    [
      {
        "id" => 1,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4001",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 2,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4002",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 3,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4002",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 4,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4002",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 5,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 6,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 7,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 8,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 9,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      },
      {
        "id" => 10,
        "house_id" => 1,
        "fog_error_type" => FogError::FOG_ERROR_TYPE_NODE_FAILURE,
        "failed_port" => "4003",
        "reported_at_timestamp" => Time.now.to_i
      }
    ]
  )

end