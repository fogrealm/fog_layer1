class CreateServiceTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :service_tasks do |t|
      t.references :service_infra, index: true
      t.references :infra_reading, index: true

      t.integer :service_priority, index: true, default: ServiceInfra::MIN_PRIORITY
      t.integer :gateway_timestamp

      t.integer :retry_count, index: true
      t.integer :max_retry_count, index: true
      t.boolean :is_drop_allowed, default: false
      t.string :service_task_type, default: ServiceTask::SERVICE_TASK_TYPE_OTHERS, index: true

      t.timestamps
    end
  end
end
