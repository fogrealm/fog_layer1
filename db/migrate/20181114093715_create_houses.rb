class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.string :name
      t.text :address
      t.string :landmark
      t.text :description

      t.string :authentication_token, unique: true

      t.integer :icfn_id
      t.string :icfn_base_url
      t.string :icfn_channel_key

      t.integer :cloud_id
      t.string :cloud_base_url
      t.string :cloud_channel_key

      t.timestamps
    end
  end
end
