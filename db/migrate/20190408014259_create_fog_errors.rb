class CreateFogErrors < ActiveRecord::Migration[5.0]
  def change
    create_table :fog_errors do |t|
      t.references :house, index: true

      t.string :fog_error_type, default: FogError::FOG_ERROR_TYPE_NODE_FAILURE, index: true
      t.string :failed_port, index: true

      t.datetime :reported_at

      t.timestamps
    end
  end
end
