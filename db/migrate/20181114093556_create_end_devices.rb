class CreateEndDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :end_devices do |t|
      t.references :house_section, index: true
      t.references :service_infra, index: true
      t.references :end_device_info, index: true
      t.references :device_gateway, index: true

      t.string :name
      t.boolean :should_alert_status, default: true
      t.string :serial, unique: true

      t.timestamps
    end
  end
end
