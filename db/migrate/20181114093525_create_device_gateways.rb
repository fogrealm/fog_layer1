class CreateDeviceGateways < ActiveRecord::Migration[5.0]
  def change
    create_table :device_gateways do |t|
      
      t.string :name
      t.string :device_gateway_url
      t.integer :device_gateway_id, index: true
      t.string :channel_key
      t.string :channel_encryption_scheme

      t.timestamps
    end
  end
end
