class AddBaseUrlToDeviceGateways < ActiveRecord::Migration[5.0]
  def change
    add_column :device_gateways, :base_url, :string
  end
end
