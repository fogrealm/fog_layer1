class RenameSerialToSerialNumberInEndDevices < ActiveRecord::Migration[5.0]
  def change
    rename_column :end_devices, :serial, :serial_number
  end
end
